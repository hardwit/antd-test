import React from 'react'

import { ConfigProvider } from 'antd'

import { HomePage } from "./pages"
import { GlobalStyle } from "./styled.global"
import { LIGHT_THEME } from "./themes/light"


function App() {
  const currentTheme = LIGHT_THEME

  return (
    <ConfigProvider
      theme={currentTheme}
    >
      <GlobalStyle />

      <HomePage />
    </ConfigProvider>
  )
}

export default App
