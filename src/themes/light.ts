export const LIGHT_THEME = {
  token: {
    fontSize: 14,
    colorTextDisabled: '#CFCFCF'
  },
  components: {
    Menu: {
      itemColor: '#002C51'
    }
  }
}
