import React from 'react'

import styled from 'styled-components'
import { Card, Typography } from "antd"
import paymentCardImg from 'images/payment-card.png'

interface IProps {
  className?: string
}

export const PaymentsCards: React.FC<IProps> = ({ className }) => {
  const cards = [
    {
      date: 'Период: 01.01.2024 — 31.12.2024',
      amount: '2 732 545',
      currency: '₽',
      label: 'Всего по договору',
      id: 1
    },
    {
      date: 'Дата: 17.03.2024',
      amount: '325 457',
      currency: '₽',
      label: 'Платёж просрочен',
      id: 2
    },
    {
      date: 'Дата: 29.03.2024',
      amount: '271 295',
      currency: '₽',
      label: 'Ближайший платёж',
      id: 3
    },
  ]

  return (
    <SPaymentsCardsBox className={className}>
      {
        cards.map(card => (
          <SCard key={card.id}>
            <SCardImg src={paymentCardImg} />

            <SDateText disabled>
              {card.date}
            </SDateText>

            <SAmountTextBox>
              <SAmountText>
                {card.amount}
              </SAmountText>

              {' '}

              <SAmountCurrencyText>
                {card.currency}
              </SAmountCurrencyText>
            </SAmountTextBox>

            <SLabelText>
              {card.label}
            </SLabelText>
          </SCard>
        ))
      }
    </SPaymentsCardsBox>
  )
}

const SPaymentsCardsBox = styled.div`
  display: flex;
`

const SCard = styled(Card)`
  && {
    flex: 1;
    
    .ant-card-body {
      padding: 24px;
      position: relative;
      display: flex;
      flex-direction: column;
    }
    
    & ~ & {
      margin-left: 24px;
    }
  }
`

const SCardImg = styled.img`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 24px;
  right: 24px;
`

const SDateText = styled(Typography.Text)`
  font-size: 15px;
`

const SAmountTextBox = styled.p`
  margin: 24px 0 0;
`

const SAmountText = styled(Typography.Text)`
  && {
    color: #002C51;
    font-size: 40px;
    font-weight: 600;
    line-height: 1;
  }
`

const SAmountCurrencyText = styled(Typography.Text)`
  && {
    color: #002C51;
    font-size: 40px;
    font-weight: 400;
    line-height: 1;
  }
`

const SLabelText = styled(Typography.Text)`
  && {
    margin-top: 4px;
    color: #002C51;
  }
`
