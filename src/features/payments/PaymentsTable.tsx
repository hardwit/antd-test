import React from "react"

import { Select, SelectProps, Table } from "antd"
import styled from "styled-components"

type LabelRender = SelectProps['labelRender'];

interface IProps {
  className?: string
}

enum PaymentStatuses {
  paid = 'paid',
  unknown = 'unknown',
  'not-paid' = 'not-paid'
}

const dataSource = [
  {
    key: '1',
    id: 123,
    date: '17.03.2024',
    period: '17.03.2024 — 30.08.2024',
    amount: '5 748 592',
    finalAmount: '6 341 257',
    status: PaymentStatuses.paid,
  },
  {
    key: '2',
    id: 444,
    date: '17.03.2024',
    period: '17.03.2024 — 30.08.2024',
    amount: '5 748 592',
    finalAmount: '6 341 257',
    status: PaymentStatuses["not-paid"],
  },
  {
    key: '3',
    id: 4445,
    date: '17.03.2024',
    period: '17.03.2024 — 30.08.2024',
    amount: '5 111 111',
    finalAmount: '6 111 111',
    status: PaymentStatuses.unknown,
  },
]

const labelRender: LabelRender = (props) => {
  const { label, value } = props

  const colors: Record<string, string> = {
    [PaymentStatuses.paid]: '#52C41A',
    [PaymentStatuses["not-paid"]]: '#FF4D4F',
    [PaymentStatuses.unknown]: '#CFCFCF'
  }

  return (
    <SLabel>
      <SLabelCircle color={colors[value]} />

      {label}
    </SLabel>
  )
}

const columns = [
  {
    title: '№ платежа',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Дата платежа',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'Период оплаты',
    dataIndex: 'period',
    key: 'period',
  },
  {
    title: 'Сумма по договору',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'Сумма с учётом изменений',
    dataIndex: 'finalAmount',
    key: 'finalAmount',
  },
  {
    title: 'Статус оплаты',
    dataIndex: 'status',
    key: 'status',
    render: (status: PaymentStatuses) => (
      <SSelect variant='borderless'
        defaultValue={status}
        labelRender={labelRender}
      >
        <Select.Option value={PaymentStatuses.paid}>
          Оплачено
        </Select.Option>

        <Select.Option value={PaymentStatuses["not-paid"]}>
          Не оплачено
        </Select.Option>

        <Select.Option value={PaymentStatuses.unknown}>
          Нет статуса
        </Select.Option>
      </SSelect>
    ),
  },
]


export const PaymentsTable: React.FC<IProps> = ({ className }) => {
  return (
    <Table dataSource={dataSource}
      columns={columns}
      className={className}
    />
  )
}

const SSelect = styled(Select)`
  width: 100%;
`

const SLabel = styled.div`
  display: flex;
  align-items: center;
`

const SLabelCircle = styled.div<{color: string}>`
  margin-right: 8px;
  width: 6px;
  height: 6px;
  border-radius: 50%;
  background-color: ${(props) => props.color};
`
