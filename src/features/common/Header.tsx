import { Layout as AntdLayout } from 'antd'
import styled from "styled-components"
import logoImg from 'images/logo.png'
import profileImg from 'images/profile.png'
import { BellOutlined, QuestionCircleOutlined, SearchOutlined } from "@ant-design/icons"

export const Header = () => {
  return (
    <SHeader>
      <SLogo src={logoImg} />

      <SRightBox>
        <SIconLink>
          <SSearchOutlined />
        </SIconLink>

        <SIconLink>
          <SQuestionCircleOutlined />
        </SIconLink>

        <SIconLink>
          <SBellOutlined />

          <SNotificationsCount>
            11
          </SNotificationsCount>
        </SIconLink>

        <SProfileLink>
          <SProfileImg src={profileImg} />

          Serati Ma
        </SProfileLink>
      </SRightBox>
    </SHeader>
  )
}

const SHeader = styled(AntdLayout.Header)`
  height: 48px;
  padding: 0 60px 0 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const SLogo = styled.img`
  height: 48px;
  width: auto;
`

const SRightBox = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const SIconLink = styled.a`
  position: relative;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  
  & ~ & {
    margin-left: 24px;
  }
`

const SSearchOutlined = styled(SearchOutlined)`
  color: #FFFFFF;
`

const SQuestionCircleOutlined = styled(QuestionCircleOutlined)`
  color: #FFFFFF;
`

const SBellOutlined = styled(BellOutlined)`
  color: #FFFFFF;
`

const SNotificationsCount = styled.div`
  height: 20px;
  padding: 0 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #FF4D4F;
  position: absolute;
  top: 2px;
  right: -22px;
  border-radius: 100px;
  font-weight: 400;
  font-size: 12px;
  color: white;
`

const SProfileLink = styled.a`
  margin-left: 24px;
  display: flex;
  align-items: center;
  color: #FFFFFF;
  font-weight: 400;
  font-size: 14px;
`

const SProfileImg = styled.img`
  width: 24px;
  height: 24px;
  margin-right: 8px;
`
