import React from 'react'

import styled from 'styled-components'
import { Layout as AntdLayout } from 'antd'

import { Header } from "./Header"
import { LeftSidebar } from "./LeftSidebar"

interface ILayoutProps {
  children: React.ReactNode
}

export const Layout: React.FC<ILayoutProps> = ({ children }) => {
  return (
    <SMainAntdLayout>
      <Header />

      <AntdLayout>
        <LeftSidebar />

        <SContentLayout>
          {children}
        </SContentLayout>
      </AntdLayout>
    </SMainAntdLayout>
  )
}

const SMainAntdLayout = styled(AntdLayout)`
  min-height: 100vh;
`

const SContentLayout = styled(AntdLayout)`
  padding: 24px;
`
