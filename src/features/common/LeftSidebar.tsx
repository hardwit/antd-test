import React from 'react'

import styled from 'styled-components'
import { Layout, Menu } from 'antd'
import { MenuFoldOutlined, UserOutlined } from "@ant-design/icons"

export const LeftSidebar = () => {
  const menuItems = [
    {
      key: '1',
      icon: <UserOutlined />,
      label: 'Договоры ДМС',
    },
    {
      key: '2',
      icon: <UserOutlined />,
      label: 'Программы страхования',
    },
    {
      key: '3',
      icon: <UserOutlined />,
      label: 'Застрахованные',
    },
    {
      key: '4',
      icon: <UserOutlined />,
      label: 'Договоры ДМС',
    },
    {
      key: '5',
      icon: <UserOutlined />,
      label: 'Заявки',
    },
    {
      key: '6',
      icon: <UserOutlined />,
      label: 'Пользователи',
    },
  ]

  return (
    <SSider width={232}>
      <Menu
        mode="inline"
        defaultSelectedKeys={[]}
        inlineIndent={16}
        items={menuItems}
      />

      <SSidebarFooter>
        <SMenuFoldOutlined />
      </SSidebarFooter>
    </SSider>
  )
}

const SSider = styled(Layout.Sider)`
  && {
    background: #FFFFFF;
    padding-top: 24px;
    box-shadow: 0px 2px 8px 0px #00000026;
    
    .ant-layout-sider-children {
      display: flex;
      flex-direction: column;
    }
  }
`

const SSidebarFooter = styled.div`
  margin-top: auto;
  width: 100%;
  border-top: 1px solid #F0F0F0;
  height: 40px;
  padding: 0 16px;
  display: flex;
  align-items: center;
`

const SMenuFoldOutlined = styled(MenuFoldOutlined)`
  color: #002C51;
`
