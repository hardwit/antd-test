import { Breadcrumb, Card, Tabs, TabsProps, Tag, Typography } from "antd"
import styled from "styled-components"

import { Layout } from "../features/common/Layout"
import { PaymentsCards } from "../features/payments/PaymentsCards"
import { PaymentsTable } from "../features/payments/PaymentsTable"

export const HomePage = () => {
  const tabsItems: TabsProps['items'] = [
    {
      key: '1',
      label: 'Обзор',
      children: 'Обзор',
    },
    {
      key: '2',
      label: 'График платежей',
      children: (
        <>
          <PaymentsCards />

          <SPaymentsTable />
        </>
      ),
    },
    {
      key: '3',
      label: 'История изменений',
      children: 'История изменений',
    },
  ]

  return (
    <Layout>
      <SCard>
        <Breadcrumb
          items={[
            {
              title: 'Home',
            },
            {
              title: 'Application Center'
            },
            {
              title: 'An Application',
            },
          ]}
        />

        <STextBox>
          <STitle
            level={4}
          >
    Договор № 77-МЮ-3861-23/0193
          </STitle>

          <SText type="secondary">
            ООО «Смарт Солюшн» 11.03.2023 — 10.03.2024
          </SText>

          <STag color="green">
            Действующий
          </STag>
        </STextBox>

      </SCard>

      <STabs
        defaultActiveKey="2"
        items={tabsItems}
      />


    </Layout>
  )
}

const SCard = styled(Card)`
  && {
    .ant-card-body {
      padding: 16px 24px;
    }
  }
`

const STextBox = styled.div`
  margin-top: 14px;
  display: flex;
  align-items: center;
`

const STitle = styled(Typography.Title)`
  && {
    margin: 0;
  }
`

const SText = styled(Typography.Text)`
  margin-left: 12px;
  font-size: 14px;
`

const STag = styled(Tag)`
  && {
    margin-left: 12px;
    color: #52C41A;
  }
`

const STabs = styled(Tabs)`
  margin-top: 26px;
`

const SPaymentsTable = styled(PaymentsTable)`
  margin-top: 16px;
`
